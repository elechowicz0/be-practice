# be-practice

## Description
Be-practice is a repo for backend practices.

## Build with
- Python 3

## Run Locally

- Clone the project

```bash
  git clone https://gitlab.com/elechowicz0/be-practice.git
```

- Go to the project directory

```bash
  cd be-practice
```

- Create virtual environment

- Activate virtual environment

- Install requirements

- run project



### practice_1
Directory with a small project returning the fibonacci number for a given n.



