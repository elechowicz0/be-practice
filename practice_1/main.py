def fibonacci(n: int) -> int:
    if n <= 0:
        print("Incorrect input")
    elif n == 1:
        return 1
    elif n == 2:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

if __name__ == "__main__":
    assert fibonacci(1) == 1
    assert fibonacci(10) == 55
    assert fibonacci(14) == 377
